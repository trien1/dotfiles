# dotfiles

### please be aware that the guide written below was poorly tested and uses the aur to install packages

#### Installation guide

1. clone the repository
```
git clone https://github.com/trien1/dotfiles.git
```
2. be in the desired directory
```
cd dotfiles
```
3. give the needed permission to execute the script
```
chmod +x install.sh
```
4. run the script
```
./install.sh
```
